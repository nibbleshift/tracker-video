#include <gst/gst.h>
#include <glib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <sys/time.h>

#include <gst/rtsp-server/rtsp-server.h>

#include "gstnvdsmeta.h"

#define PGIE_CLASS_ID_VEHICLE 0
#define PGIE_CLASS_ID_PERSON 2

#define MUXER_OUTPUT_WIDTH 4000
#define MUXER_OUTPUT_HEIGHT 3000

#define MUXER_BATCH_TIMEOUT_USEC 4000000

#define GST_CAPS_FEATURES_NVMM "memory:NVMM"

gchar pgie_classes_str[4][32] = { 
	"Vehicle", 
	"TwoWheeler", 
	"Person",
	"RoadSign"
};
gboolean
link_element_to_tee_src_pad (GstElement *tee, GstElement *sinkelem)
{
  gboolean ret = FALSE;
  GstPad *tee_src_pad = NULL;
  GstPad *sinkpad = NULL;
  GstPadTemplate *padtemplate = NULL;

  padtemplate = (GstPadTemplate *)
      gst_element_class_get_pad_template (GST_ELEMENT_GET_CLASS (tee),
      "src_%u");
  tee_src_pad = gst_element_request_pad (tee, padtemplate, NULL, NULL);
  if (!tee_src_pad) {
    printf ("Failed to get src pad from tee");
    goto done;
  }

  sinkpad = gst_element_get_static_pad (sinkelem, "sink");
  if (!sinkpad) {
    printf ("Failed to get sink pad from '%s'",
        GST_ELEMENT_NAME (sinkelem));
    goto done;
  }

  if (gst_pad_link (tee_src_pad, sinkpad) != GST_PAD_LINK_OK) {
    printf ("Failed to link '%s' and '%s'", GST_ELEMENT_NAME (tee),
        GST_ELEMENT_NAME (sinkelem));
    goto done;
  }

  ret = TRUE;

done:
  if (tee_src_pad) {
    gst_object_unref (tee_src_pad);
  }
  if (sinkpad) {
    gst_object_unref (sinkpad);
  }
  return ret;
}

static GstPadProbeReturn
tiler_src_pad_buffer_probe (GstPad * pad, GstPadProbeInfo * info,
		gpointer u_data)
{
	GstBuffer *buf = (GstBuffer *) info->data;
	guint num_rects = 0; 
	NvDsObjectMeta *obj_meta = NULL;
	guint vehicle_count = 0;
	guint person_count = 0;
	NvDsMetaList * l_frame = NULL;
	NvDsMetaList * l_obj = NULL;

	NvDsBatchMeta *batch_meta = gst_buffer_get_nvds_batch_meta (buf);

	for (l_frame = batch_meta->frame_meta_list; l_frame != NULL;
			l_frame = l_frame->next) {
		NvDsFrameMeta *frame_meta = (NvDsFrameMeta *) (l_frame->data);
		for (l_obj = frame_meta->obj_meta_list; l_obj != NULL;
				l_obj = l_obj->next) {
			obj_meta = (NvDsObjectMeta *) (l_obj->data);
			if (obj_meta->class_id == PGIE_CLASS_ID_VEHICLE) {
				vehicle_count++;
				num_rects++;
			}
			if (obj_meta->class_id == PGIE_CLASS_ID_PERSON) {
				person_count++;
				num_rects++;
			}
		}
		if (person_count != 0 || vehicle_count != 0) {
			g_print ("Frame Number = %d Number of objects = %d "
					"Vehicle Count = %d Person Count = %d\n",
					frame_meta->frame_num, num_rects, vehicle_count, person_count);
		}

	}
	return GST_PAD_PROBE_OK;
}

static gboolean
bus_call (GstBus * bus, GstMessage * msg, gpointer data)
{
	GMainLoop *loop = (GMainLoop *) data;
	switch (GST_MESSAGE_TYPE (msg)) {
		case GST_MESSAGE_EOS:
			g_print ("End of stream\n");
			g_main_loop_quit (loop);
			break;
		case GST_MESSAGE_WARNING:
			{
				gchar *debug;
				GError *error;
				gst_message_parse_warning (msg, &error, &debug);
				g_printerr ("WARNING from element %s: %s\n",
						GST_OBJECT_NAME (msg->src), error->message);
				g_free (debug);
				g_printerr ("Warning: %s\n", error->message);
				g_error_free (error);
				break;
			}
		case GST_MESSAGE_ERROR:
			{
				gchar *debug;
				GError *error;
				gst_message_parse_error (msg, &error, &debug);
				g_printerr ("ERROR from element %s: %s\n",
						GST_OBJECT_NAME (msg->src), error->message);
				if (debug)
					g_printerr ("Error details: %s\n", debug);
				g_free (debug);
				g_error_free (error);
				g_main_loop_quit (loop);
				break;
			}
		default:
			break;
	}
	return TRUE;
}

static void
cb_newpad (GstElement * decodebin, GstPad * decoder_src_pad, gpointer data)
{
	g_print ("In cb_newpad\n");
	GstCaps *caps = gst_pad_get_current_caps (decoder_src_pad);
	const GstStructure *str = gst_caps_get_structure (caps, 0);
	const gchar *name = gst_structure_get_name (str);
	GstElement *source_bin = (GstElement *) data;
	GstCapsFeatures *features = gst_caps_get_features (caps, 0);

	if (!strncmp (name, "video", 5)) {
		if (gst_caps_features_contains (features, GST_CAPS_FEATURES_NVMM)) {
			GstPad *bin_ghost_pad = gst_element_get_static_pad (source_bin, "src");
			if (!gst_ghost_pad_set_target (GST_GHOST_PAD (bin_ghost_pad),
						decoder_src_pad)) {
				g_printerr ("Failed to link decoder src pad to source bin ghost pad\n");
			}
			gst_object_unref (bin_ghost_pad);
		} else {
			g_printerr ("Error: Decodebin did not pick nvidia decoder plugin.\n");
		}
	} 
}

static void
decodebin_child_added (GstChildProxy * child_proxy, GObject * object,
		gchar * name, gpointer user_data)
{
	g_print ("Decodebin child added: %s\n", name);
	if (g_strrstr (name, "decodebin") == name) {
		g_signal_connect (G_OBJECT (object), "child-added",
				G_CALLBACK (decodebin_child_added), user_data);
	}
	if (g_strstr_len (name, -1, "nvv4l2decoder") == name) {
		g_print ("Seting bufapi_version\n");
		g_object_set (object, "bufapi-version", TRUE, NULL);
	}
}

static GstElement *
create_source_bin (guint index, gchar * uri)
{
	GstElement *bin = NULL, *uri_decode_bin = NULL;
	gchar bin_name[16] = { };

	g_snprintf (bin_name, 15, "source-bin-%02d", index);
	bin = gst_bin_new (bin_name);

	uri_decode_bin = gst_element_factory_make ("uridecodebin", "uri-decode-bin");

	if (!bin || !uri_decode_bin) {
		g_printerr ("One element in source bin could not be created.\n");
		return NULL;
	}

	g_object_set (G_OBJECT (uri_decode_bin), "uri", uri, NULL);

	g_signal_connect (G_OBJECT (uri_decode_bin), "pad-added",
			G_CALLBACK (cb_newpad), bin);
	g_signal_connect (G_OBJECT (uri_decode_bin), "child-added",
			G_CALLBACK (decodebin_child_added), bin);

	gst_bin_add (GST_BIN (bin), uri_decode_bin);

	if (!gst_element_add_pad (bin, gst_ghost_pad_new_no_target ("src",
					GST_PAD_SRC))) {
		g_printerr ("Failed to add ghost pad in source bin\n");
		return NULL;
	}

	return bin;
}

typedef struct {
	GstElement *pipeline;
	GstElement *src;
	GstElement *parse;
	GstElement *tee;

	char *rtsp_uri;
} CorePipeline;

typedef struct {
	/* video inference pipeline */
	GstElement *sink;
	GstElement *stream_queue;
	GstElement *mux;
	GstElement *pgie;
	GstElement *nvvidconv;
	GstElement *nvosd;
	GstElement *transform;
} StreamPipeline;

typedef struct {
	/* record pipeline */
	GstElement *filesink;
	GstElement *file_queue;
} RecordPipeline;

StreamPipeline *create_stream_pipeline(CorePipeline *core);
CorePipeline *create_core_pipeline(char *rtsp_uri);
RecordPipeline *create_record_pipeline(CorePipeline *core);

static void on_pad_added(GstElement *element, GstPad *pad, void *data)
{
	CorePipeline *core = (CorePipeline *) data;
	RecordPipeline *record = NULL;
	StreamPipeline *stream = NULL;
	GstPad *sinkpad;

	printf("on_pad_added\n");

	sinkpad = gst_element_get_static_pad(core->tee, "sink");

	GstPadLinkReturn ret = gst_pad_link(pad, sinkpad);

	if (GST_PAD_LINK_FAILED(ret)) {
		printf("Link failed: %d\n", ret);
	}

	g_object_unref(sinkpad);

	stream = create_stream_pipeline(core);
	
	gst_bin_add_many (GST_BIN (core->pipeline), 
			stream->pgie, 
			stream->nvvidconv, 
			stream->nvosd, 
			stream->transform, 
			stream->sink,
			NULL);

	if (!gst_element_link_many (stream->mux, stream->pgie, stream->nvvidconv, stream->nvosd, stream->transform, stream->sink,
				NULL)) {
		g_printerr ("Failed linking stream pipeline\n");
		return ;
	}
	link_element_to_tee_src_pad (core->tee, stream->stream_queue);

	record = create_record_pipeline(core);
	
	gst_bin_add_many(GST_BIN(core->pipeline), record->file_queue, record->filesink, NULL);

	if (!gst_element_link_many (core->tee, record->file_queue, record->filesink, NULL)) {
		g_printerr ("Failed linking stream pipeline\n");
		return ;
	}

	GST_DEBUG_BIN_TO_DOT_FILE(GST_BIN(core->pipeline), GST_DEBUG_GRAPH_SHOW_ALL, "pipeline");
}



RecordPipeline *create_record_pipeline(CorePipeline *core) {
	RecordPipeline *p = NULL;
	
	p = malloc(sizeof(RecordPipeline));
	/* Record pipeline */
	p->filesink = gst_element_factory_make("filesink", "filesink");

	if (!p->filesink) {
		g_printerr ("failed creating filesink\n");
		return NULL;
	}
	g_object_set(p->filesink, "location", "./recorder.mp4", NULL);

	p->file_queue = gst_element_factory_make("queue", "file_queue");

	if (!p->file_queue) {
		g_printerr ("failed creating file_queue\n");
		return NULL;
	}

	return p;
}

CorePipeline *create_core_pipeline(char *rtsp_uri) {
	CorePipeline *p = NULL;
	
	p = malloc(sizeof(CorePipeline));
	p->rtsp_uri = strdup(rtsp_uri);
	p->pipeline = gst_pipeline_new ("tracker-pipeline");

	if (!p->pipeline) {
		g_printerr ("One element could not be created. Exiting.\n");
		return NULL;
	}

	/* src will be our rtsp stream that is coming in */
	p->src = gst_element_factory_make ("rtspsrc", "src");

	if (!p->src) {
		g_printerr ("One element could not be created. Exiting.\n");
		return NULL;
	}

	printf("rstp: %s\n", rtsp_uri);
	g_object_set(G_OBJECT (p->src), "location", rtsp_uri, NULL);

	/* this will a tee so we can branch the pipeline */
	p->tee = gst_element_factory_make("tee", "tee");

	if (!p->tee) {
		g_printerr ("One element could not be created. Exiting.\n");
		return NULL;
	}
	g_object_set(p->tee, "allow-not-linked", TRUE, NULL);

	p->parse = gst_element_factory_make("parsebin", "parse");

	if (!p->parse) {
		g_printerr ("One element could not be created. Exiting.\n");
		return NULL;
	}


	gst_bin_add_many(GST_BIN(p->pipeline), p->src, p->tee, NULL);

	printf("Core=%p\n", p);

	return p;
}

StreamPipeline *create_stream_pipeline(CorePipeline *core) 
{
	StreamPipeline *p = NULL;
	guint i;
	guint pgie_batch_size = 1;
	
	p = malloc(sizeof(StreamPipeline));
	
	p->stream_queue = gst_element_factory_make("queue", "stream_queue");

	if (!p->stream_queue) {
		g_printerr ("failed creating stream_queue\n");
		return NULL;
	}

	p->mux = gst_element_factory_make ("nvstreammux", "stream-muxer");

	p->pgie = gst_element_factory_make ("nvinfer", "primary-nvinference-engine");

	p->nvvidconv = gst_element_factory_make ("nvvideoconvert", "nvvideo-converter");

	p->nvosd = gst_element_factory_make ("nvdsosd", "nv-onscreendisplay");

	p->transform = gst_element_factory_make ("nvegltransform", "nvegl-transform");

	p->sink = gst_element_factory_make ("nveglglessink", "nvvideo-renderer");

	if (!p->pgie || !p->nvvidconv || !p->nvosd || !p->sink) {
		g_printerr ("One element could not be created. Exiting.\n");
		return NULL;
	}

	if(!p->transform) {
		g_printerr ("One tegra element could not be created. Exiting.\n");
		return NULL;
	}

	GstPad *sinkpad, *srcpad;
	gchar pad_name[16] = { };
	GstElement *source_bin = create_source_bin (0, core->rtsp_uri);

	gst_bin_add (GST_BIN (core->pipeline), p->mux);

	if (!source_bin) {
		g_printerr ("Failed to create source bin. Exiting.\n");
		return NULL;
	}

	gst_bin_add (GST_BIN (core->pipeline), p->stream_queue);

	g_snprintf (pad_name, 15, "sink_%u", 0);
	sinkpad = gst_element_get_request_pad (p->mux, pad_name);
	if (!sinkpad) {
		g_printerr ("Streammux request sink pad failed. Exiting.\n");
		return NULL;
	}

	srcpad = gst_element_get_static_pad (p->stream_queue, "src");
	if (!srcpad) {
		g_printerr ("Failed to get src pad of source bin. Exiting.\n");
		return NULL;
	}
	
	GstPadProbeReturn ret;
	ret = gst_pad_link (srcpad, sinkpad);
	if (ret != GST_PAD_LINK_OK) {
		g_printerr ("Failed to link source bin to stream muxer. %d Exiting.\n", ret);
		return NULL;
	}

	gst_object_unref (srcpad);
	gst_object_unref (sinkpad);


	g_object_set (G_OBJECT (p->pgie),
			"config-file-path", "tracker_config.txt", NULL);

	g_object_get (G_OBJECT (p->pgie), "batch-size", &pgie_batch_size, NULL);

	return p;
}
int main (int argc, char *argv[])
{
	GMainLoop *loop = NULL;
	CorePipeline *core = NULL;

	GstBus *bus = NULL;
	guint bus_watch_id;

	if (argc < 2) {
		g_printerr ("Usage: %s <rtspsrc>\n", argv[0]);
		return -1;
	}

	gst_init (&argc, &argv);
	loop = g_main_loop_new (NULL, FALSE);

	core = create_core_pipeline(argv[1]);

	bus = gst_pipeline_get_bus (GST_PIPELINE (core->pipeline));
	bus_watch_id = gst_bus_add_watch (bus, bus_call, loop);
	gst_object_unref (bus);

	
	/* src will be our rtsp stream that is coming in */
	g_signal_connect(core->src, "pad-added", G_CALLBACK (on_pad_added), core);
	printf("Registered\n");

	gst_element_set_state (core->pipeline, GST_STATE_PLAYING);
	g_main_loop_run (loop);

	GST_DEBUG_BIN_TO_DOT_FILE(GST_BIN(core->pipeline), GST_DEBUG_GRAPH_SHOW_ALL, "pipeline");
	gst_element_set_state (core->pipeline, GST_STATE_NULL);
	gst_object_unref (GST_OBJECT (core->pipeline));
	g_source_remove (bus_watch_id);
	g_main_loop_unref (loop);
	return 0;
}
