#include <gst/gst.h>
#include <glib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <sys/time.h>

#include <gst/rtsp-server/rtsp-server.h>

#include "gstnvdsmeta.h"
//#include "gstnvstreammeta.h"

#define PGIE_CLASS_ID_VEHICLE 0
#define PGIE_CLASS_ID_PERSON 2

#define MUXER_OUTPUT_WIDTH 4000
#define MUXER_OUTPUT_HEIGHT 3000

#define MUXER_BATCH_TIMEOUT_USEC 4000000
static GstPadProbeReturn
tiler_src_pad_buffer_probe (GstPad * pad, GstPadProbeInfo * info,
    gpointer u_data)
{
    GstBuffer *buf = (GstBuffer *) info->data;
    guint num_rects = 0;
    NvDsObjectMeta *obj_meta = NULL;
    guint vehicle_count = 0;
    guint person_count = 0;
    NvDsMetaList * l_frame = NULL;
    NvDsMetaList * l_obj = NULL;

    NvDsBatchMeta *batch_meta = gst_buffer_get_nvds_batch_meta (buf);
	
    for (l_frame = batch_meta->frame_meta_list; l_frame != NULL;
      l_frame = l_frame->next) {
        NvDsFrameMeta *frame_meta = (NvDsFrameMeta *) (l_frame->data);
        //int offset = 0;
        for (l_obj = frame_meta->obj_meta_list; l_obj != NULL;
                l_obj = l_obj->next) {
            obj_meta = (NvDsObjectMeta *) (l_obj->data);
            if (obj_meta->class_id == PGIE_CLASS_ID_VEHICLE) {
                vehicle_count++;
                num_rects++;
            }
            if (obj_meta->class_id == PGIE_CLASS_ID_PERSON) {
                person_count++;
                num_rects++;
            }
        }
                  g_print ("Frame Number = %d Number of objects = %d "
                    "Vehicle Count = %d Person Count = %d\n",
                    frame_meta->frame_num, num_rects, vehicle_count, person_count);

    }
    return GST_PAD_PROBE_OK;
}

static gboolean
bus_call (GstBus * bus, GstMessage * msg, gpointer data)
{
	GMainLoop *loop = (GMainLoop *) data;
	switch (GST_MESSAGE_TYPE (msg)) {
		case GST_MESSAGE_EOS:
			g_print ("End of stream\n");
			g_main_loop_quit (loop);
			break;
		case GST_MESSAGE_WARNING:
			{
				gchar *debug;
				GError *error;
				gst_message_parse_warning (msg, &error, &debug);
				g_printerr ("WARNING from element %s: %s\n",
						GST_OBJECT_NAME (msg->src), error->message);
				g_free (debug);
				g_printerr ("Warning: %s\n", error->message);
				g_error_free (error);
				break;
			}
		case GST_MESSAGE_ERROR:
			{
				gchar *debug;
				GError *error;
				gst_message_parse_error (msg, &error, &debug);
				g_printerr ("ERROR from element %s: %s\n",
						GST_OBJECT_NAME (msg->src), error->message);
				if (debug)
					g_printerr ("Error details: %s\n", debug);
				g_free (debug);
				g_error_free (error);
				g_main_loop_quit (loop);
				break;
			}
		default:
			break;
	}
	return TRUE;
}

static int link_tee_to_queue(GstElement *tee, GstElement *queue, int index) 
{
	GstPad *sinkpad, *srcpad;
	gchar pad_name_sink[16] = "sink";
	gchar pad_name_src[16]; 

	sprintf(pad_name_src, "src_%u", index);

	sinkpad = gst_element_get_static_pad (queue, pad_name_sink);

	if (!sinkpad) {
		g_printerr ("tee request sink pad failed. Exiting.\n");
		return -1;
	}

	srcpad = gst_element_get_request_pad (tee, pad_name_src);

	if (!srcpad) {
		g_printerr ("tee request src pad failed. Exiting.\n");
		return -1;
	}

	if (gst_pad_link (srcpad, sinkpad) != GST_PAD_LINK_OK) {
		g_printerr ("tee(%d) Failed linkin.\n", index);
		return -1;
	}

	gst_object_unref (sinkpad);
	gst_object_unref (srcpad);
}

static int link_streamux_to_decode(GstElement *streammux, GstElement *decode) 
{
	GstPad *sinkpad, *srcpad;
	gchar pad_name_sink[16] = "sink_0";
	gchar pad_name_src[16] = "src";

	sinkpad = gst_element_get_request_pad (streammux, pad_name_sink);
	if (!sinkpad) {
		g_printerr ("Streammux request sink pad failed. Exiting.\n");
		return -1;
	}

	srcpad = gst_element_get_static_pad (decode, pad_name_src);

	if (!srcpad) {
		g_printerr ("link: decode request src pad failed. Exiting.\n");
		return -1;
	}

	GstPadLinkReturn ret = gst_pad_link (srcpad, sinkpad);
	if (ret != GST_PAD_LINK_OK) {
		g_printerr ("Failed to link decoder to stream muxer. Exiting. %d\n", ret);
		return -1;
	}

	gst_object_unref (sinkpad);
	gst_object_unref (srcpad);
}

GstElement *pipeline;

static void on_pad_added(GstElement *element, GstPad *pad, void *data)
{
        GstElement *depay = (GstElement *) data;
	GstElement *queue = NULL;
        GstPad *sinkpad;

        printf("on_pad_added\n");


        sinkpad = gst_element_get_static_pad(depay, "sink");

        GstPadLinkReturn ret = gst_pad_link(pad, sinkpad);

        if (GST_PAD_LINK_FAILED(ret)) {
                printf("Link failed: %d\n", ret);
        }

        g_object_unref(sinkpad);

	GST_DEBUG_BIN_TO_DOT_FILE(GST_BIN(pipeline), GST_DEBUG_GRAPH_SHOW_ALL, "pipeline");
	//gst_element_link_many(element, parse, NULL);
}

int main (int argc, char *argv[])
{
	GMainLoop *loop = NULL;
	GstElement *streammux = NULL;
	GstElement *decode = NULL;
	GstElement *parse = NULL;

	GstElement *depay = NULL;
	GstElement *tee = NULL;
	GstElement *queue = NULL;
	GstElement *file_queue = NULL;
	GstBus *bus = NULL;
	GstElement *src = NULL;
	GstElement *pgie = NULL;
	GstElement *nvvidconv = NULL;
	GstElement *nvvidconv2 = NULL;
	GstElement *nvosd = NULL;
	GstElement *transform = NULL;
	GstElement *render = NULL;

	GstPad *tiler_src_pad = NULL;

	// output to hls
	GstElement *encode = NULL;
	GstElement *tsmux = NULL;
	GstElement *h264parse2 = NULL;
	GstElement *hlssink = NULL;

	gint bus_watch_id;
	gint pgie_batch_size;

	if (argc < 2) {
		g_printerr ("Usage: %s <uri1>\n", argv[0]);
		return -1;
	}

	gst_init (&argc, &argv);
	loop = g_main_loop_new (NULL, FALSE);

	pipeline = gst_pipeline_new ("pipeline");

        /* src will be our rtsp stream that is coming in */
        src = gst_element_factory_make ("rtspsrc", "src");

        if (!src) {
                g_printerr ("One element could not be created. Exiting.\n");
                return -1;
        }
	gst_bin_add(GST_BIN(pipeline),src);
        printf("rstp: %s\n", argv[1]);
        g_object_set(G_OBJECT (src), "location", argv[1], NULL);


	depay = gst_element_factory_make ("rtph264depay", "rtph264depay");
	gst_bin_add (GST_BIN (pipeline), depay);

	parse = gst_element_factory_make ("h264parse", "h264-parser");
	gst_bin_add (GST_BIN (pipeline), parse);


	streammux = gst_element_factory_make ("nvstreammux", "stream-muxer");

	if (!pipeline || !streammux) {
		g_printerr ("One element could not be created. Exiting.\n");
		return -1;
	}
	gst_bin_add (GST_BIN (pipeline), streammux);

	queue = gst_element_factory_make ("queue", "stream-queue");

	if (!queue) {
		g_printerr ("One element could not be created. Exiting.\n");
		return -1;
	}
	gst_bin_add (GST_BIN (pipeline), queue);

	tee = gst_element_factory_make ("tee", "tee");

	if (!tee) {
		g_printerr ("One element could not be created. Exiting.\n");
		return -1;
	}
	gst_bin_add (GST_BIN (pipeline), tee);

	gst_element_link_many(depay, parse, tee, NULL);

	/* file pipeline */
	file_queue = gst_element_factory_make ("queue", "file-queue");

	if (!file_queue) {
		g_printerr ("One element could not be created. Exiting.\n");
		return -1;
	}
	gst_bin_add(GST_BIN(pipeline), file_queue);
        
	link_tee_to_queue(tee, file_queue, 0);
	link_tee_to_queue(tee, queue, 1);


	decode = gst_element_factory_make ("nvv4l2decoder", "decode");

	if (!decode) {
		g_printerr ("One element could not be created. Exiting.\n");
		return -1;
	}
	gst_bin_add (GST_BIN (pipeline), decode);

	g_object_set (G_OBJECT (streammux), "width", MUXER_OUTPUT_WIDTH, "height",
			MUXER_OUTPUT_HEIGHT, "batch-size", 1,
			"batched-push-timeout", MUXER_BATCH_TIMEOUT_USEC, NULL);



	GstPad *sinkpad, *srcpad;

	sinkpad = gst_element_get_static_pad (decode, "sink");
	if (!sinkpad) {
		g_printerr ("decode request sink pad failed. Exiting.\n");
		return -1;
	}

	srcpad = gst_element_get_static_pad (queue, "src");

	if (!srcpad) {
		g_printerr ("decode request src pad failed. Exiting.\n");
		return -1;
	}

	if (gst_pad_link (srcpad, sinkpad) != GST_PAD_LINK_OK) {
		g_printerr ("Failed linking queue to decode. Exiting.\n");
		return -1;
	}

	gst_object_unref (sinkpad);
	gst_object_unref (srcpad);

	link_streamux_to_decode(streammux, decode);

	/* Use nvinfer to infer on batched frame. */
	pgie = gst_element_factory_make ("nvinfer", "primary-nvinference-engine");

	/* Use convertor to convert from NV12 to RGBA as required by nvosd */
	nvvidconv = gst_element_factory_make ("nvvideoconvert", "nvvideo-converter");
	nvvidconv2 = gst_element_factory_make ("nvvideoconvert", "nvvideo-converter2");

	/* Create OSD to draw on the converted RGBA buffer */
	nvosd = gst_element_factory_make ("nvdsosd", "nv-onscreendisplay");

	encode = gst_element_factory_make("nvv4l2h264enc", "encode");

	tsmux = gst_element_factory_make("mpegtsmux", "tsmux");

	h264parse2 = gst_element_factory_make("h264parse", "h264parse2");

	hlssink = gst_element_factory_make("hlssink", "hlssink");


	if (!pgie || !nvvidconv || !nvosd) {
		g_printerr ("One element could not be created. Exiting.\n");
		return -1;
	}

	/* we add a message handler */
	bus = gst_pipeline_get_bus (GST_PIPELINE (pipeline));
	bus_watch_id = gst_bus_add_watch (bus, bus_call, loop);
	gst_object_unref (bus);

	gst_bin_add_many (GST_BIN (pipeline), pgie, nvvidconv, nvosd, nvvidconv2,
			NULL);
	gst_bin_add_many (GST_BIN (pipeline), encode, tsmux, h264parse2, hlssink,
			NULL);
	/* we link the elements together
	 * nvstreammux -> nvinfer -> nvvidconv -> nvosd -> video-renderer */

	g_object_set (G_OBJECT (hlssink), "max-files", 10, "target-duration", 10, NULL);

	g_object_set (G_OBJECT (streammux), "width", MUXER_OUTPUT_WIDTH, "height",
			MUXER_OUTPUT_HEIGHT, "batch-size", 1,
			"batched-push-timeout", MUXER_BATCH_TIMEOUT_USEC, NULL);

	/* Configure the nvinfer element using the nvinfer config file. */
	g_object_set (G_OBJECT (pgie),
			"config-file-path", "tracker_config.txt", NULL);

	/* Override the batch-size set in the config file with the number of sources. */
	g_object_get (G_OBJECT (pgie), "batch-size", &pgie_batch_size, NULL);
	if (pgie_batch_size != 1) {
		g_printerr
			("WARNING: Overriding infer-config batch-size (%d) with number of sources (%d)\n",
			 pgie_batch_size, 1);
		g_object_set (G_OBJECT (pgie), "batch-size", 1, NULL);
	}

	/* Set up the pipeline */
	/* we add all elements into the pipeline */
	if (!gst_element_link_many ( streammux, pgie, nvvidconv, nvosd, nvvidconv2, encode, h264parse2, tsmux, hlssink,
				NULL)) {
		g_printerr ("Elements could not be linked. Exiting.\n");
		return -1;
	}

	tiler_src_pad = gst_element_get_static_pad (pgie, "src");
	if (!tiler_src_pad)
		g_print ("Unable to get src pad\n");
	else
		gst_pad_add_probe (tiler_src_pad, GST_PAD_PROBE_TYPE_BUFFER,
				tiler_src_pad_buffer_probe, NULL, NULL);



	g_signal_connect(src, "pad-added", G_CALLBACK (on_pad_added), depay);

	gst_element_set_state (pipeline, GST_STATE_PLAYING);
	g_main_loop_run (loop);

	/* Out of the main loop, clean up nicely */
	g_print ("Returned, stopping playback\n");
	gst_element_set_state (pipeline, GST_STATE_NULL);
	g_print ("Deleting pipeline\n");
	gst_object_unref (GST_OBJECT (pipeline));
	g_source_remove (bus_watch_id);
	g_main_loop_unref (loop);
	return 0;
}
